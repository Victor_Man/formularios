package com.example.pc.formularios;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.itextpdf.text.DocumentException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FormularioActivity extends AppCompatActivity {
Context context;

    //Variables y clase PDF
    InvoiceObject invoiceObject = new InvoiceObject();
    //nombre pdf
    private String FILENAME = "EmproacsaEjemplo.pdf";
    //carpeta
    private String APP_FOLDER_NAME = "com.emproacsa.pdf";
    //subcarpeta
    private String INVOICES_FOLDER = "Emproacsa";
    //Declaramos la clase PdfManager
     PdfManager pdfManager = null;
    //ruta sdcard
    String sdCardRoot = Environment.getExternalStorageDirectory().getPath();
    //ruta
    String path= sdCardRoot + File.separator + APP_FOLDER_NAME+ File.separator;
    //-------------------------
    //variables privadas
    //private Formulario fo = new Formulario();
    private ArrayList<Formulario> formularioArrayList;
    private String codigo_For, guardareditarFor;
    //ruta de un archivo

    //Variables de la vista
    ImageButton fechabutton, horabutton;
    Button firmaEmproacsabt,firmaRepresentantebt,firmaResponsablebt,guardarbt,visualizarbt,enviarbt;
    EditText fechatext,horatext,actuaciontext,firmaResponsabletext, firmaRepresentantetext,firmaEmproacsatext,observacionestext,subcontratistatext,dirigidotext,situaciontext,promotortext;
    CheckBox promotorbox,contratistabox,coordinadorbox,tecnicodtobox,instalacionebox,otrosbox,direccionfacultativabox,subcontratistaEmpresabox,replanteobox,demolicionbox,movimientotierrasbox,cimentacionbox,estructurabox,cubiertabox,cerramientobox,acabadosbox;
    //Variables a pasar a la base de datos
    String actuacion,firmaResponsable,firmaRepresentante,firmaEmproacsa,fecha,hora,observaciones,dirigido,subcontratista,situacion,promotor;
    Boolean promotorB,contratista,direccion,subcontratistaB,replanteo,demolicion,movimientoTierra,cimentacion,estructura,cerramientos,cubierta,acabados,instalaciones,otros,coordinadorSegSal,tecnicoDtoPre=false;
    //Formato de la fecha y la hora por separado
    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yy");
    Calendar Date = Calendar.getInstance();
    DateFormat formatTime = DateFormat.getTimeInstance(DateFormat.SHORT);
    Calendar Time = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_formulario);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
        @Override
          public void onClick(View v) {
            onBackPressed();
           }
        });
        //---------------------------------------------PARTE-FECHA-Y-HORA-------------------------------------------------------
        //Variables de la vista
        fechatext = (EditText) findViewById(R.id.fechaText);
        fechabutton = (ImageButton) findViewById(R.id.fechaButton);
        horatext = (EditText) findViewById(R.id.horaText);
        horabutton = (ImageButton) findViewById(R.id.horaButton);
        fechatext.setEnabled(false);
        horatext.setEnabled(false);
        //Al pulsar llamara al metodo que abre el DatePickerDialog de la fecha
        fechabutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDate();
            }
        });
        //Al pulsar llamara al metodo que abre el DatePickerDialog de la hora
        horabutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTime();
            }
        });
        //Aun que no se pulse los botones la fecha y hora se pondran por defecto
        updateTextFechaLabel();
        updateTextHoraLabel();
        //----------------------------------------------------------------------------------------------------------------------
        //BOTONES
        guardarbt = (Button) findViewById(R.id.btGuardar);
        enviarbt = (Button) findViewById(R.id.btEnviarEmail);
        visualizarbt = (Button) findViewById(R.id.btVisualizar);
        firmaEmproacsabt = (Button) findViewById(R.id.btFirmaEmproacsa);
        firmaRepresentantebt = (Button) findViewById(R.id.btFirmaRepresentante);
        firmaResponsablebt = (Button) findViewById(R.id.btFirmaResponsable);
        //EDIT TEXT
        actuaciontext = (EditText) findViewById(R.id.Ed_actuacion);
        promotortext = (EditText) findViewById(R.id.Edpromotor);
        situaciontext = (EditText) findViewById(R.id.Edsituacion);
        dirigidotext = (EditText) findViewById(R.id.Eddirigidoa);
        subcontratistatext = (EditText) findViewById(R.id.Edsubcontratista);
        observacionestext = (EditText) findViewById(R.id.Edobservaciones);
        firmaEmproacsatext = (EditText) findViewById(R.id.firmaEmproacsa);
        firmaRepresentantetext = (EditText) findViewById(R.id.firmaRepresentante);
        firmaResponsabletext = (EditText) findViewById(R.id.firmaResponsable);
        //CHECKBOX
        promotorbox = (CheckBox) findViewById(R.id.CBpromotor);
        contratistabox = (CheckBox) findViewById(R.id.CBcontratista);
        direccionfacultativabox = (CheckBox) findViewById(R.id.CBdireccionfacultativa);
        subcontratistaEmpresabox = (CheckBox) findViewById(R.id.CBsubcontratistaEmpresa);
        replanteobox = (CheckBox) findViewById(R.id.CBreplanteo);
        demolicionbox = (CheckBox) findViewById(R.id.CBdemolicion);
        movimientotierrasbox = (CheckBox) findViewById(R.id.CBmovimientotierras);
        cimentacionbox = (CheckBox) findViewById(R.id.CBcimentacion);
        estructurabox = (CheckBox) findViewById(R.id.CBestructura);
        cubiertabox = (CheckBox) findViewById(R.id.CBcubierta);
        cerramientobox = (CheckBox) findViewById(R.id.CBcerramiento);
        acabadosbox = (CheckBox) findViewById(R.id.CBacabados);
        instalacionebox = (CheckBox) findViewById(R.id.CBinstalacione);
        otrosbox = (CheckBox) findViewById(R.id.CBotros);
        coordinadorbox = (CheckBox) findViewById(R.id.CBcoordinador);
        tecnicodtobox = (CheckBox) findViewById(R.id.CBtecnicodto);

        //---------------------------------------------------------FIRMA-------------------------------------------------------------
        firmaResponsablebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = context.getSharedPreferences("firmante", Context.MODE_PRIVATE);
                SharedPreferences.Editor edito = pref.edit();
                edito.putString("firmado","Responsable");
                edito.commit();
                Intent intent1=new Intent(FormularioActivity.this, FirmarActivity.class);
                startActivity(intent1);
            }
        });
        firmaRepresentantebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = context.getSharedPreferences("firmante", Context.MODE_PRIVATE);
                SharedPreferences.Editor edito = pref.edit();
                edito.putString("firmado","Representante");
                edito.commit();
                Intent intent1=new Intent(FormularioActivity.this, FirmarActivity.class);
                startActivity(intent1);
            }
        });
        firmaEmproacsabt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = context.getSharedPreferences("firmante", Context.MODE_PRIVATE);
                SharedPreferences.Editor edito = pref.edit();
                edito.putString("firmado","Emproacsa");
                edito.commit();
                Intent intent1=new Intent(FormularioActivity.this, FirmarActivity.class);
                startActivity(intent1);
            }
        });
        //---------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------PDF---------------------------------------------------------------
        //Creamos una factura desde nuestro código solo para poder generar el documento PDF
        //con esta información

        try {
            //Instanciamos la clase PdfManager
            pdfManager = new PdfManager(context);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        visualizarbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Mostrar PDF
                try {

                assert pdfManager != null;
                    //ruta
                    String fileName = INVOICES_FOLDER+ File.separator + codigo_For +FILENAME;
                    File file = new File(path + fileName);
                        if(file.exists()){
                        pdfManager.showPdfFile(fileName,FormularioActivity.this);
                        }else {
                            createInvoiceObject(codigo_For,RecuperarFormulario(codigo_For));
                            pdfManager.createPdfDocument(invoiceObject);
                            pdfManager.showPdfFile(fileName,FormularioActivity.this);
                        }
                    }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        enviarbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailTo ="victormanuelgomu@gmail.com";
                //ruta
                String fileName = INVOICES_FOLDER + File.separator + codigo_For + FILENAME;
                File file = new File(path + fileName);
                assert pdfManager != null;
                if(file.exists()){
                    pdfManager.sendPdfByEmail(INVOICES_FOLDER + File.separator +codigo_For+FILENAME,emailTo,null, FormularioActivity.this);
                }else {
                    createInvoiceObject(codigo_For,RecuperarFormulario(codigo_For));
                    pdfManager.createPdfDocument(invoiceObject);
                    pdfManager.sendPdfByEmail(INVOICES_FOLDER + File.separator +codigo_For+FILENAME,emailTo,null, FormularioActivity.this);
                }
            }
        });
        //---------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------NUEVO-O-EDITAR-------------------------------------------------------------------------
        SharedPreferences prefs = this.getSharedPreferences("ModoGuardarEditar", Context.MODE_PRIVATE);
        guardareditarFor = prefs.getString("guardareditar","0");

        //Modo Editar
        if(guardareditarFor.equals("editar")){
            SharedPreferences pref = this.getSharedPreferences("MiCodigo", Context.MODE_PRIVATE);
            codigo_For = pref.getString("codigo","0");
            guardarbt.setText("Editar");
            RellenarFormulario(RecuperarFormulario(codigo_For));

            guardarbt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditarFormulario(codigo_For);
                    guardarbt.setEnabled(false);
                    String fileName = INVOICES_FOLDER + File.separator + codigo_For +FILENAME;
                    File file = new File(path + fileName);
                    file.delete();
                    firmaEmproacsabt.setEnabled(false);
                    firmaRepresentantebt.setEnabled(false);
                    firmaResponsablebt.setEnabled(false);
                }
            });
            subcontratistaEmpresabox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subcontrata();
                }
            });
        }
        //Modo Nuevo
        else if(guardareditarFor.equals("guardar")){
            enviarbt.setEnabled(false);
            visualizarbt.setEnabled(false);
            //Si el campo esta plulsado se podra editar sino no
            if (subcontratistaEmpresabox.isChecked()) {
                subcontratistatext.setEnabled(true);
            } else {
                subcontratistatext.setEnabled(false);
            }

            guardarbt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GuardarDatos();
                    guardarbt.setEnabled(false);
                    enviarbt.setEnabled(true);
                    visualizarbt.setEnabled(true);
                    firmaEmproacsabt.setEnabled(false);
                    firmaRepresentantebt.setEnabled(false);
                    firmaResponsablebt.setEnabled(false);
                }
            });
            subcontratistaEmpresabox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subcontrata();
                }
            });
        }
        //------------------------------------------------------------------------------------------------------------------------------

    }
    //---------------------------------------------PARTE-FECHA-Y-HORA-----------------------------------------------------------
    // Metodo que abre el DatePinckDialog de la fecha
    private void updateDate(){
        new DatePickerDialog(this, d, Date.get(Calendar.YEAR),Date.get(Calendar.MONTH),Date.get(Calendar.DAY_OF_MONTH)).show();
    }
    // Metodo que abre el DatePinckDialog de la hora
    private void updateTime(){
        new TimePickerDialog(this, t, Time.get(Calendar.HOUR_OF_DAY), Time.get(Calendar.MINUTE), true).show();
    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Date.set(Calendar.YEAR, year);
            Date.set(Calendar.MONTH, monthOfYear);
            Date.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateTextFechaLabel();
        }
    };

    TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Time.set(Calendar.HOUR_OF_DAY, hourOfDay);
            Time.set(Calendar.MINUTE, minute);
            updateTextHoraLabel();
        }
    };

    private void updateTextHoraLabel(){
        horatext.setText(formatTime.format(Time.getTime()));
    }
    private void updateTextFechaLabel(){
        fechatext.setText(formatDate.format(Date.getTime()));
    }
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void subcontrata(){
        if(subcontratistaEmpresabox.isChecked()){
            subcontratistatext.setEnabled(true);
        }else{
            subcontratistatext.setEnabled(false);
            subcontratistatext.setText("");
        }
    }
    //-----------------------------------------------------GUARDAR--------------------------------------------------------------------
    public void GuardarDatos() {

        actuacion = actuaciontext.getText().toString();
        fecha = fechatext.getText().toString();
        hora = horatext.getText().toString();
        firmaResponsable = firmaResponsabletext.getText().toString();
        firmaRepresentante = firmaRepresentantetext.getText().toString();
        firmaEmproacsa = firmaEmproacsatext.getText().toString();
        observaciones = observacionestext.getText().toString();
        dirigido = dirigidotext.getText().toString();
        subcontratista = subcontratistatext.getText().toString();
        situacion = situaciontext.getText().toString();
        promotor = promotortext.getText().toString();

        if (promotorbox.isChecked()) {
            promotorB = true;
        } else {
            promotorB = false;
        }
        if (contratistabox.isChecked()) {
            contratista = true;
        } else {
            contratista = false;
        }
        if (direccionfacultativabox.isChecked()) {
            direccion = true;
        } else {
            direccion = false;
        }
        if (subcontratistaEmpresabox.isChecked()) {
            subcontratistaB = true;
        } else {
            subcontratistaB = false;
        }
        if (replanteobox.isChecked()) {
            replanteo = true;
        } else {
            replanteo = false;
        }
        if (demolicionbox.isChecked()) {
            demolicion = true;
        } else {
            demolicion = false;
        }
        if (movimientotierrasbox.isChecked()) {
            movimientoTierra = true;
        } else {
            movimientoTierra = false;
        }
        if (cimentacionbox.isChecked()) {
            cimentacion = true;
        } else {
            cimentacion = false;
        }
        if (estructurabox.isChecked()) {
            estructura = true;
        } else {
            estructura = false;
        }
        if (cerramientobox.isChecked()) {
            cerramientos = true;
        } else {
            cerramientos = false;
        }
        if (cubiertabox.isChecked()) {
            cubierta = true;
        } else {
            cubierta = false;
        }
        if (acabadosbox.isChecked()) {
            acabados = true;
        } else {
            acabados = false;
        }
        if (instalacionebox.isChecked()) {
            instalaciones = true;
        } else {
            instalaciones = false;
        }
        if (otrosbox.isChecked()) {
            otros = true;
        } else {
            otros = false;
        }
        if (coordinadorbox.isChecked()) {
            coordinadorSegSal = true;
        } else {
            coordinadorSegSal = false;
        }
        if (tecnicodtobox.isChecked()) {
            tecnicoDtoPre = true;
        } else {
            tecnicoDtoPre = false;
        }
        try {
            FormularioSQLite formularioSQLite = new FormularioSQLite(this,"formularios", null, 3);
            SQLiteDatabase db = formularioSQLite.getWritableDatabase();
            //Si hemos abierto correctamente la base de datos
            if (db != null) {
                ContentValues valores = new ContentValues();
                valores.put("actuacion", actuacion);
                valores.put("fecha", fecha);
                valores.put("hora", hora);
                valores.put("promotor", promotor);
                valores.put("situacion", situacion);
                valores.put("dirigido", dirigido);
                valores.put("promotorcheck", promotorB);
                valores.put("contratista", contratista);
                valores.put("direccion", direccion);
                valores.put("subcontratista", subcontratistaB);
                valores.put("empresa_subcon", subcontratista);
                valores.put("replanteo", replanteo);
                valores.put("demolicion", demolicion);
                valores.put("movimientoTierra", movimientoTierra);
                valores.put("cimentacion", cimentacion);
                valores.put("estructura", estructura);
                valores.put("cerramientos", cerramientos);
                valores.put("cubierta", cubierta);
                valores.put("acabados", acabados);
                valores.put("instalaciones", instalaciones);
                valores.put("otros", otros);
                valores.put("observaciones", observaciones);
                valores.put("firmaEmproacsa", firmaEmproacsa);
                valores.put("coordinadorSegSal", coordinadorSegSal);
                valores.put("tecnicoDtoPre", tecnicoDtoPre);
                valores.put("firmaRepresentante", firmaRepresentante);
                valores.put("firmaResponsable", firmaResponsable);
                valores.put("urlfirmaEmproacsa","");
                valores.put("urlfirmaRepresentante","");
                valores.put("urlfirmaResponsable","");

                //insertar
                db.insertOrThrow("vista", null, valores);
                db.close();
                Toast.makeText(this,"El formulario se ha guardado corectamente",Toast.LENGTH_LONG).show();
            } else {
                AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                alerta.setTitle(this.getString(R.string.error));
                alerta.setMessage("Error en el registro");
                alerta.show();
                db.close();
            }
        } catch (Exception e) {

            AlertDialog.Builder alerta = new AlertDialog.Builder(this);
            alerta.setTitle(this.getString(R.string.error));
            alerta.setMessage("Error" +e);
            alerta.show();
        }
        //Guardar el codigo del formulario en una variable para usar en el pdf
        try{
            FormularioSQLite formularioSQLite = new FormularioSQLite(this,"formularios", null, 3);
            SQLiteDatabase db = formularioSQLite.getReadableDatabase();
            String[] args = new String[]{fecha,hora};
            //Si hemos abierto correctamente la base de datos
            if (db != null) {
                Cursor cursor = db.rawQuery("SELECT codigo FROM vista WHERE fecha=? and hora=?", args);
                if (cursor.getCount() > 0) {
                    if (cursor.moveToFirst()) {
                        //guardamos el codigo en la variable local
                        codigo_For=cursor.getString(cursor.getColumnIndex("codigo"));
                    }
                }
                db.close();
                urlfirma(codigo_For);
            }else {
                AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                alerta.setTitle(this.getString(R.string.error));
                alerta.setMessage("Error en el registro");
                alerta.show();
                db.close();
            }
        }catch (Exception e) {
            AlertDialog.Builder alerta = new AlertDialog.Builder(this);
            alerta.setTitle(this.getString(R.string.error));
            alerta.setMessage("Error" +e);
            alerta.show();
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------MODIFICAR-URLFIRMA--------------------------------------------------------
    //METODO QUE CAMBIA EL NOMBRE DE LAS FIRMAS Y LES PONE SU CODIGO DE FORMULARIO
    public void urlfirma(String codigo){
        String[] firmas = new String[3];
        firmas[0]="Emproacsa";
        firmas[1]="Responsable";
        firmas[2]="Representante";
        int size = firmas.length;
        for (int i=0; i<size; i++) {
            String rutafirma = R.string.carpetafirma + File.separator  + firmas[i] + "firma.png";
            String rutafirmacodigo = R.string.carpetafirma + File.separator  +codigo+ firmas[i] + "firma.png";
            File From = new File(path + rutafirma);
            File To = new File(path + rutafirmacodigo);
            //mirar base de datos
            if(From.exists()) {
                From.renameTo(To);
                insertarUrl(rutafirmacodigo, firmas[i], codigo);
            }

        }
    }
//METODO QUE INSERTA EN BBDD LA RUTA DE LA FIRMA
    public void insertarUrl(String ruta, String campo, String codigo){
        try{
        FormularioSQLite formularioSQLite = new FormularioSQLite(this, "formularios", null, 3);
        SQLiteDatabase db = formularioSQLite.getWritableDatabase();
        //Si hemos abierto correctamente la base de datos
        if (db != null) {
            ContentValues valores = new ContentValues();
            valores.put("urlfirma"+campo, ruta);
            db.update("vista", valores, "codigo="+codigo, null);
            db.close();
        } else {
        AlertDialog.Builder alerta = new AlertDialog.Builder(this);
        alerta.setTitle(this.getString(R.string.error));
        alerta.setMessage("Error en el registro");
        alerta.show();
        db.close();
    }
        } catch (Exception e) {

            AlertDialog.Builder alerta = new AlertDialog.Builder(this);
            alerta.setTitle(this.getString(R.string.error));
            alerta.setMessage("Error" +e);
            alerta.show();
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------RECUPERAR DATOS FORMULARIO---------------------------------------------------------------------
    //En este metodo pasandole el codigo del formulario sacamos todos los datos
    public ArrayList<Formulario> RecuperarFormulario(String codigo){
        try {
            FormularioSQLite formularioSQLite = new FormularioSQLite(this, "formularios", null, 3);
            SQLiteDatabase db = formularioSQLite.getReadableDatabase();
            String[] args = new String[]{codigo};
            if (db != null) {
                Cursor cursor = db.rawQuery(" SELECT actuacion,fecha,hora,promotor,situacion,dirigido,promotorcheck,contratista,direccion,subcontratista,empresa_subcon,replanteo,demolicion,movimientoTierra,cimentacion,estructura,cerramientos,cubierta,acabados,instalaciones,otros,observaciones,firmaEmproacsa,coordinadorSegSal,tecnicoDtoPre,firmaRepresentante,firmaResponsable, urlfirmaEmproacsa, urlfirmaResponsable, urlfirmaRepresentante FROM vista WHERE codigo=? ", args);
                //actuacion,promotor,situacion,dirigido,promotorcheck,contratista,direccion,subcontratista,empresa_subcon,replanteo,demolicion,movimientoTierra,cimentacion,estructura,cerramientos,cubierta,acabados,instalaciones,otros,observaciones,firmaEmproacsa,coordinadorSegSal,tecnicoDtoPre,firmaRepresentante,firmaResponsable
                if (cursor.getCount() > 0) {
                    if (cursor.moveToFirst()) {
                        actuacion=cursor.getString(cursor.getColumnIndex("actuacion"));
                        fecha=cursor.getString(cursor.getColumnIndex("fecha"));
                        hora=cursor.getString(cursor.getColumnIndex("hora"));
                        promotor=cursor.getString(cursor.getColumnIndex("promotor"));
                        situacion=cursor.getString(cursor.getColumnIndex("situacion"));
                        dirigido=cursor.getString(cursor.getColumnIndex("dirigido"));

                        if (cursor.getInt(cursor.getColumnIndex("promotorcheck")) !=0) {
                            promotorB=true;
                        }else{
                            promotorB=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("contratista")) !=0) {
                            contratista=true;
                        }else{
                            contratista=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("direccion")) !=0) {
                            direccion=true;
                        }else{
                            direccion=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("subcontratista")) !=0) {
                            subcontratistaB=true;
                        }else{
                            subcontratistaB=false;
                        }
                        subcontratista=cursor.getString(cursor.getColumnIndex("empresa_subcon"));

                        if (cursor.getInt(cursor.getColumnIndex("replanteo"))!=0) {
                            replanteo=true;
                        }else{
                            replanteo=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("demolicion")) !=0) {
                            demolicion=true;
                        }else{
                            demolicion=false;
                        }if (cursor.getInt(cursor.getColumnIndex("movimientoTierra")) !=0) {
                            movimientoTierra=true;
                        }else{
                            movimientoTierra=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("cimentacion")) !=0) {
                            cimentacion=true;
                        }else{
                            cimentacion=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("estructura"))!=0) {
                            estructura=true;
                        }else{
                            estructura=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("cerramientos")) !=0) {
                            cerramientos=true;
                        }else{
                            cerramientos=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("cubierta")) !=0) {
                            cubierta=true;
                        }else{
                            cubierta=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("acabados")) !=0) {
                            acabados=true;
                        }else{
                            acabados=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("instalaciones")) !=0) {
                            instalaciones=true;
                        }else{
                            instalaciones=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("otros")) !=0) {
                            otros=true;
                        }else{
                            otros=false;
                        }
                        observaciones=cursor.getString(cursor.getColumnIndex("observaciones"));
                        firmaEmproacsa=cursor.getString(cursor.getColumnIndex("firmaEmproacsa"));
                        if (cursor.getInt(cursor.getColumnIndex("coordinadorSegSal")) !=0) {
                            coordinadorSegSal=true;
                        }else{
                            coordinadorSegSal=false;
                        }
                        if (cursor.getInt(cursor.getColumnIndex("tecnicoDtoPre")) !=0) {
                            tecnicoDtoPre=true;
                        }else{
                            tecnicoDtoPre=false;
                        }
                        firmaRepresentante=cursor.getString(cursor.getColumnIndex("firmaRepresentante"));
                        firmaResponsable=cursor.getString(cursor.getColumnIndex("firmaResponsable"));
                        db.close();
                         }
                }
            } else {
                AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                alerta.setTitle(this.getString(R.string.error));
                alerta.setMessage("Error en la base de datos");
                alerta.show();
                db.close();
            }
        }catch (Exception e){
            AlertDialog.Builder alerta = new AlertDialog.Builder(this);
            alerta.setTitle(this.getString(R.string.error));
            alerta.setMessage("Error" +e);
            alerta.show();
        }
        formularioArrayList = new ArrayList<>();
        formularioArrayList.add(new Formulario(actuacion,firmaResponsable,firmaRepresentante,firmaEmproacsa,fecha,hora,observaciones,dirigido,subcontratista,situacion,promotor,promotorB,contratista,direccion,subcontratistaB,replanteo,demolicion,movimientoTierra,cimentacion,estructura,cerramientos,cubierta,acabados,instalaciones,otros,coordinadorSegSal,tecnicoDtoPre));
        return formularioArrayList;
    }
    //-------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------RELLENAR-FORMULARIO--------------------------------------------------------------------
    public void RellenarFormulario(ArrayList<Formulario> formularioArrayList) {


        for (Formulario f : formularioArrayList) {
            actuaciontext.setText(f.getActuacion());
            promotortext.setText(f.getPromotor());
            situaciontext.setText(f.getSituacion());
            dirigidotext.setText(f.getDirigido());

            if (f.getPromotorB()) {
                promotorbox.setChecked(true);
            }
            if ( f.getContratista()){
                contratistabox.setChecked(true);
            }
            if (f.getDireccion()){
                direccionfacultativabox.setChecked(true);
            }
            if ( f.getSubcontratistaB()){
                subcontratistaEmpresabox.setChecked(true);
            }
            subcontratistatext.setText(f.getSubcontratista());
            if (f.getReplanteo()){
                replanteobox.setChecked(true);
            }
            if (f.getDemolicion()){
                demolicionbox.setChecked(true);
            }
            if (f.getMovimientoTierra()){
                movimientotierrasbox.setChecked(true);
            }
            if ( f.getCimentacion()){
                cimentacionbox.setChecked(true);
            }
            if (f.getEstructura()){
                estructurabox.setChecked(true);
            }
            if (f.getCerramientos()){
                cerramientobox.setChecked(true);
            }
            if ( f.getCubierta()){
                cubiertabox.setChecked(true);
            }
            if (f.getAcabados()){
                acabadosbox.setChecked(true);
            }
            if (f.getInstalaciones()){
                instalacionebox.setChecked(true);
            }
            if (f.getOtros()){
                otrosbox.setChecked(true);
            }
            observacionestext.setText(f.getObservaciones());
            firmaEmproacsatext.setText(f.getFirmaEmproacsa());
            if (f.getCoordinadorSegSal()){
                coordinadorbox.setChecked(true);
            }
            if (f.getTecnicoDtoPre()){
                tecnicodtobox.setChecked(true);
            }
            firmaRepresentantetext.setText(f.getFirmaRepresentante());
            firmaResponsabletext.setText(f.getFirmaResponsable());
        }
    }
    //----------------------------------------------------REGUARDAR-FORMULARIO-----------------------------------------------------------
        public void EditarFormulario(String codigo){
            //Codigo del formulario a editar

            //Volcamos los datos de los campos a las variables
            actuacion = actuaciontext.getText().toString();
            fecha = fechatext.getText().toString();
            hora = horatext.getText().toString();
            firmaResponsable = firmaResponsabletext.getText().toString();
            firmaRepresentante = firmaRepresentantetext.getText().toString();
            firmaEmproacsa = firmaEmproacsatext.getText().toString();
            observaciones = observacionestext.getText().toString();
            dirigido = dirigidotext.getText().toString();
            subcontratista = subcontratistatext.getText().toString();
            situacion = situaciontext.getText().toString();
            promotor = promotortext.getText().toString();

            if (promotorbox.isChecked()) {
                promotorB = true;
            } else {
                promotorB = false;
            }
            if (contratistabox.isChecked()) {
                contratista = true;
            } else {
                contratista = false;
            }
            if (direccionfacultativabox.isChecked()) {
                direccion = true;
            } else {
                direccion = false;
            }
            if (subcontratistaEmpresabox.isChecked()) {
                subcontratistaB = true;
            } else {
                subcontratistaB = false;
            }
            if (replanteobox.isChecked()) {
                replanteo = true;
            } else {
                replanteo = false;
            }
            if (demolicionbox.isChecked()) {
                demolicion = true;
            } else {
                demolicion = false;
            }
            if (movimientotierrasbox.isChecked()) {
                movimientoTierra = true;
            } else {
                movimientoTierra = false;
            }
            if (cimentacionbox.isChecked()) {
                cimentacion = true;
            } else {
                cimentacion = false;
            }
            if (estructurabox.isChecked()) {
                estructura = true;
            } else {
                estructura = false;
            }
            if (cerramientobox.isChecked()) {
                cerramientos = true;
            } else {
                cerramientos = false;
            }
            if (cubiertabox.isChecked()) {
                cubierta = true;
            } else {
                cubierta = false;
            }
            if (acabadosbox.isChecked()) {
                acabados = true;
            } else {
                acabados = false;
            }
            if (instalacionebox.isChecked()) {
                instalaciones = true;
            } else {
                instalaciones = false;
            }
            if (otrosbox.isChecked()) {
                otros = true;
            } else {
                otros = false;
            }
            if (coordinadorbox.isChecked()) {
                coordinadorSegSal = true;
            } else {
                coordinadorSegSal = false;
            }
            if (tecnicodtobox.isChecked()) {
                tecnicoDtoPre = true;
            } else {
                tecnicoDtoPre = false;
            }
            try {
                FormularioSQLite formularioSQLite = new FormularioSQLite(this,"formularios", null, 3);
                SQLiteDatabase db = formularioSQLite.getWritableDatabase();
                if (db != null) {
                    ContentValues valores = new ContentValues();
                    valores.put("actuacion", actuacion);
                    valores.put("fecha", fecha);
                    valores.put("hora", hora);
                    valores.put("promotor", promotor);
                    valores.put("situacion", situacion);
                    valores.put("dirigido", dirigido);
                    valores.put("promotorcheck", promotorB);
                    valores.put("contratista", contratista);
                    valores.put("direccion", direccion);
                    valores.put("subcontratista", subcontratistaB);
                    valores.put("empresa_subcon", subcontratista);
                    valores.put("replanteo", replanteo);
                    valores.put("demolicion", demolicion);
                    valores.put("movimientoTierra", movimientoTierra);
                    valores.put("cimentacion", cimentacion);
                    valores.put("estructura", estructura);
                    valores.put("cerramientos", cerramientos);
                    valores.put("cubierta", cubierta);
                    valores.put("acabados", acabados);
                    valores.put("instalaciones", instalaciones);
                    valores.put("otros", otros);
                    valores.put("observaciones", observaciones);
                    valores.put("firmaEmproacsa", firmaEmproacsa);
                    valores.put("coordinadorSegSal", coordinadorSegSal);
                    valores.put("tecnicoDtoPre", tecnicoDtoPre);
                    valores.put("firmaRepresentante", firmaRepresentante);
                    valores.put("firmaResponsable", firmaResponsable);
                    //update
                    db.update("vista", valores, "codigo="+codigo, null);
                    db.close();
                    Toast.makeText(this,"El formulario se ha editado corectamente",Toast.LENGTH_LONG).show();
                    urlfirma(codigo);
                }else {
                    AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                    alerta.setTitle(this.getString(R.string.error));
                    alerta.setMessage("Error en la base de datos");
                    alerta.show();
                    db.close();
                }
            }catch (Exception e){
                AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                alerta.setTitle(this.getString(R.string.error));
                alerta.setMessage("Error" +e);
                alerta.show();
            }
        }
    //-----------------------------------------------------------onBackPressed---------------------------------------------------------------------
    //preguntar al user si desea salir
    @Override
    public void onBackPressed() {
        if(guardarbt.isEnabled()==false){
            Intent intent1=new Intent(FormularioActivity.this, MenuActivity.class);
            startActivity(intent1);
            finish();
        }else {
        new AlertDialog.Builder(this)
                .setTitle("¿Desea Salir?")
                .setMessage("Si no ha guardado los datos, se perderan ")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        BorrarFirmas();
                        Intent intent1=new Intent(FormularioActivity.this, MenuActivity.class);
                        startActivity(intent1);
                        finish();
                    }
                }).create().show();
        }
    }
    //----------------------------------------------------BORRAR----------------------------------------------------------------------

    //borrar de la base de datos un formulario
    public void Borrar(){
        SharedPreferences prefs = this.getSharedPreferences("MiCodigo", Context.MODE_PRIVATE);
        codigo_For = prefs.getString("codigo","0");
        try {
            FormularioSQLite formularioSQLite = new FormularioSQLite(this,"formularios", null, 3);
            SQLiteDatabase db = formularioSQLite.getWritableDatabase();
            if (db != null) {
                db.delete("vista", "codigo=" + codigo_For, null);
                db.close();
                Toast.makeText(this,"El formulario en ddbb ha sido borrado corectamente",Toast.LENGTH_LONG).show();
            }else {
                AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                alerta.setTitle(this.getString(R.string.error));
                alerta.setMessage("Error en la base de datos");
                alerta.show();
                db.close();
            }
            //borrado pdf de un formulario
            String fileName = INVOICES_FOLDER + File.separator + codigo_For + FILENAME;
            File file = new File(path + fileName);
            if(file.exists()) {
                file.delete();
                Toast.makeText(this,"El formulario PDF ha sido borrado corectamente",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,"El formulario PDF no existia",Toast.LENGTH_LONG).show();
            }
            //borrar las firmas de un formulario
            BorrarFirmascodigo(codigo_For);
        }catch (Exception e){
            AlertDialog.Builder alerta = new AlertDialog.Builder(this);
            alerta.setTitle(this.getString(R.string.error));
            alerta.setMessage("Error" +e);
            alerta.show();
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------BORRAR-FIRMAS-------------------------------------------------------
    //borrado de las firmas porsi se sale del formulario sin guardar
    public void BorrarFirmas() {
        String[] firmas = new String[3];
        firmas[0]="Emproacsa";
        firmas[1]="Responsable";
        firmas[2]="Representante";
        int size = firmas.length;
        for (int i=0; i<size; i++){
            String rutafirma =  R.string.carpetafirma + File.separator + firmas[i] + "firma.png";
            File file = new File(path + rutafirma);
            if (file.exists()) {
                file.delete();
            }
        }
    }
    //borrar las firmas de un formulario en concreto
    public void BorrarFirmascodigo(String codigo) {
        String[] firmas = new String[3];
        firmas[0]="Emproacsa";
        firmas[1]="Responsable";
        firmas[2]="Representante";
        int size = firmas.length;
        for (int i=0; i<size; i++){
            String rutafirma =  R.string.carpetafirma + File.separator + codigo + firmas[i] + "firma.png";
            File file = new File(path + rutafirma);
            if (file.exists()) {
                file.delete();
            }
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------TOOLBAR-MENU------------------------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Si ha entrado para editar se mostrara el menu del toolbar si es por uno nuevo no
        SharedPreferences pref = this.getSharedPreferences("ModoGuardarEditar", Context.MODE_PRIVATE);

        guardareditarFor = pref.getString("guardareditar","0");
        if(guardareditarFor.equals("editar")) {
            getMenuInflater().inflate(R.menu.menu_formulario, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            //de verdad quiere borrar
            new AlertDialog.Builder(this)
                    .setTitle("¿Seguro que quieres borrar?")
                    .setMessage("Si borras se perderan los datos.")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            //llamamos a borrar ese formulario
                            Borrar();
                            Intent intent1=new Intent(FormularioActivity.this, MenuActivity.class);
                            startActivity(intent1);
                            finish();
                        }
                    }).create().show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //-------------------------------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------PDF--------------------------------------------------------------------
    //Creando el pdf con los datos del formulario
    private void createInvoiceObject(String Codigo, ArrayList<Formulario> formularioArrayList){
        invoiceObject.codigo=Codigo;
        for (Formulario f : formularioArrayList) {
        invoiceObject.actuacion=f.getActuacion();
        invoiceObject.promotor=f.getPromotor();
        invoiceObject.situacion=f.getSituacion();
        invoiceObject.dirigido=f.getDirigido();
        invoiceObject.fecha=f.getFecha();
        invoiceObject.hora=f.getHora();
        }
    }
    //-------------------------------------------------------------------------------------------------------------------------------
}