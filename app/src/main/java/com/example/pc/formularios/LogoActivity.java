package com.example.pc.formularios;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class LogoActivity extends AppCompatActivity {
    //Constante para la solicitud del permiso en ejecucion
    private final int MY_PERMISSIONS = 100;
    private RelativeLayout mRlView;
    String tiempoborrado;
    //nombre pdf
    private String FILENAME = "EmproacsaEjemplo.pdf";
    //carpeta
    private String APP_FOLDER_NAME = "com.emproacsa.pdf";
    //subcarpeta
    private String INVOICES_FOLDER = "Emproacsa";
    int diasborrado;
    Context contexto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);
        contexto=this;
        //----------------------------------------Sarenpreferent---------------------------------------------------------------
        SharedPreferences sp= contexto.getSharedPreferences("tiempopref",Context.MODE_PRIVATE);
        tiempoborrado=sp.getString("tiempopref","1");

        if(tiempoborrado.equals("1 Semana")){
            diasborrado=7;
        }else if(tiempoborrado.equals("2 Semanas")){
            diasborrado=14;
        }else if(tiempoborrado.equals("1 Mes")){
            diasborrado=30;
        }else if(tiempoborrado.equals("2 Meses")){
            diasborrado=60;
        }else if(tiempoborrado.equals("3 Meses")){
            diasborrado=90;
        }else if(tiempoborrado.equals("Nunca")){
            diasborrado=-1;
        }
        //-----------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------FILTRADO-----------------------------------------------------------
        SharedPreferences pref = contexto.getSharedPreferences("filtro", Context.MODE_PRIVATE);
        SharedPreferences.Editor edito = pref.edit();
        edito.putString("actuacion","");
        edito.putString("fechaI","");
        edito.putString("fechaF","");
        edito.putString("situacion","");
        edito.commit();
        //-----------------------------------------------------------------------------------------------------------------------
        mRlView = (RelativeLayout) findViewById(R.id.activity_logo2);
        if (mayRequestStoragePermission()){
           SacarFechayCodigo();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    startActivity(new Intent(getApplicationContext(), MenuActivity.class));
                    finish();
                }
            }, 2000);
        }else{

        }
    }

    private boolean mayRequestStoragePermission() {

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED))
            return true;

        if((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))){
            Snackbar.make(mRlView, "Los permisos son necesarios para poder usar la aplicación",
                    Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {
                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS);
                }
            }).show();
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS);
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS){
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(LogoActivity.this, "Permisos aceptados", Toast.LENGTH_SHORT).show();
                SacarFechayCodigo();
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        startActivity(new Intent(getApplicationContext(), MenuActivity.class));
                        finish();
                    }
                }, 2000);
            }else{
                showExplanation();
            }
        }

    }

    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LogoActivity.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Para usar las funciones de la app necesitas aceptar los permisos");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.show();
    }
//------------------------------------------------------FECHA-Y-CODIGO------------------------------------------------------------
    public void SacarFechayCodigo(){
        if(diasborrado>0) {
            try {
                FormularioSQLite formularioSQLite = new FormularioSQLite(this, "formularios", null, 3);
                SQLiteDatabase db = formularioSQLite.getReadableDatabase();
                if (db != null) {
                    Cursor cursor = db.rawQuery(" SELECT codigo,fecha FROM vista", null);
                    if (cursor.getCount() > 0) {
                        if (cursor.moveToFirst()) {

                            do {
                                String codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                                String fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                                SimpleDateFormat Dateformat = new SimpleDateFormat("dd/MM/yy");
                                Date fechaInicio = Dateformat.parse(fecha);
                                ComprobarFecha(fechaInicio, codigo);
                            }
                            while (cursor.moveToNext());
                            db.close();
                        }
                    } else {
                        Toast.makeText(LogoActivity.this, "No se han borrado Formularios automaticamente", Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                alerta.setTitle(this.getString(R.string.error));
                alerta.setMessage("Error" + e);
                alerta.show();
            }
        }
    }
    public void ComprobarFecha(Date fechainicio, String codigo){
            if( getDiffDates(fechainicio)>=diasborrado){
                BorrarFormulario(codigo);
        }
    }

    public long getDiffDates(Date fechaInicio) {

        Date hoy = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(fechaInicio);
        Date fecha = new Date(calendar.getTimeInMillis());
        // Total Dias (se calcula a partir de los milisegundos por día)
        long millsecsPerDay = 86400000; // Milisegundos al día
        long returnValue = (hoy.getTime() - fecha.getTime()) / millsecsPerDay;
        // System.out.println("Total días: " + returnValue + " Días.");
        return returnValue;
    }
    //-----------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------BORRAR-------------------------------------------------------------
    public void BorrarFormulario(String codigo){
            try {
                FormularioSQLite formularioSQLite = new FormularioSQLite(this,"formularios", null, 3);
                SQLiteDatabase db = formularioSQLite.getWritableDatabase();
                if (db != null) {
                    db.delete("vista", "codigo=" + codigo, null);
                    db.close();
                    Toast.makeText(this,"El formulario en ddbb ha sido borrado corectamente",Toast.LENGTH_LONG).show();
                }else {
                    AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                    alerta.setTitle(this.getString(R.string.error));
                    alerta.setMessage("Error en la base de datos en borrado");
                    alerta.show();
                    db.close();
                }
                //borrado pdf de un formulario
                String sdCardRoot = Environment.getExternalStorageDirectory().getPath();
                String path= sdCardRoot + File.separator + APP_FOLDER_NAME + File.separator;
                String fileName = INVOICES_FOLDER + File.separator + codigo + FILENAME;
                File file = new File(path + fileName);
                if(file.exists()) {
                    file.delete();
                    Toast.makeText(this,"El formulario PDF ha sido borrado corectamente",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(this,"El formulario PDF no existia",Toast.LENGTH_LONG).show();
                }
                //borrar las firmas de un formulario
                BorrarFirmascodigo(codigo);
            }catch (Exception e){
                AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                alerta.setTitle(this.getString(R.string.error));
                alerta.setMessage("Error" +e);
                alerta.show();
            }
        }
        //borrar las firmas de un formulario en concreto
        public void BorrarFirmascodigo(String codigo) {
            String sdCardRoot = Environment.getExternalStorageDirectory().getPath();
            String path= sdCardRoot + File.separator + APP_FOLDER_NAME + File.separator;
            String[] firmas = new String[3];
            firmas[0]="Emproacsa";
            firmas[1]="Responsable";
            firmas[2]="Representante";
            int size = firmas.length;
            for (int i=0; i<size; i++){
                String rutafirma =  getString(R.string.carpetafirma) + File.separator + codigo + firmas[i] + "firma.png";
                File file = new File(path + rutafirma);
                if (file.exists()) {
                    file.delete();
                }
            }
        }
    //---------------------------------------------------------------------------------------------------------------------------
    }
