package com.example.pc.formularios;

import java.util.ArrayList;

/**
 * Created by PC on 27/04/2017.
 */

public class Formulario {
    String actuacion,firmaResponsable,firmaRepresentante,firmaEmproacsa,fecha,hora,observaciones,dirigido,subcontratista,situacion,promotor;
    Boolean promotorB,contratista,direccion,subcontratistaB,replanteo,demolicion,movimientoTierra,cimentacion,estructura,cerramientos,cubierta,acabados,instalaciones,otros,coordinadorSegSal,tecnicoDtoPre=false;

    public Formulario() {
    }

    public Formulario(String actuacion, String firmaResponsable, String firmaRepresentante, String firmaEmproacsa, String fecha, String hora, String observaciones, String dirigido, String subcontratista, String situacion, String promotor, Boolean promotorB, Boolean contratista, Boolean direccion, Boolean subcontratistaB, Boolean replanteo, Boolean demolicion, Boolean movimientoTierra, Boolean cimentacion, Boolean estructura, Boolean cerramientos, Boolean cubierta, Boolean acabados, Boolean instalaciones, Boolean otros, Boolean coordinadorSegSal, Boolean tecnicoDtoPre) {
        this.actuacion = actuacion;
        this.firmaResponsable = firmaResponsable;
        this.firmaRepresentante = firmaRepresentante;
        this.firmaEmproacsa = firmaEmproacsa;
        this.fecha = fecha;
        this.hora = hora;
        this.observaciones = observaciones;
        this.dirigido = dirigido;
        this.subcontratista = subcontratista;
        this.situacion = situacion;
        this.promotor = promotor;
        this.promotorB = promotorB;
        this.contratista = contratista;
        this.direccion = direccion;
        this.subcontratistaB = subcontratistaB;
        this.replanteo = replanteo;
        this.demolicion = demolicion;
        this.movimientoTierra = movimientoTierra;
        this.cimentacion = cimentacion;
        this.estructura = estructura;
        this.cerramientos = cerramientos;
        this.cubierta = cubierta;
        this.acabados = acabados;
        this.instalaciones = instalaciones;
        this.otros = otros;
        this.coordinadorSegSal = coordinadorSegSal;
        this.tecnicoDtoPre = tecnicoDtoPre;
    }
    private ArrayList<Formulario> formularioArrayList= new ArrayList <> ();

    public ArrayList<Formulario> getformularioArrayList() {
        return formularioArrayList;
    }
    public String getActuacion() {
        return actuacion;
    }

    public void setActuacion(String actuacion) {
        this.actuacion = actuacion;
    }

    public String getFirmaResponsable() {
        return firmaResponsable;
    }

    public void setFirmaResponsable(String firmaResponsable) {
        this.firmaResponsable = firmaResponsable;
    }

    public String getFirmaRepresentante() {
        return firmaRepresentante;
    }

    public void setFirmaRepresentante(String firmaRepresentante) {
        this.firmaRepresentante = firmaRepresentante;
    }

    public String getFirmaEmproacsa() {
        return firmaEmproacsa;
    }

    public void setFirmaEmproacsa(String firmaEmproacsa) {
        this.firmaEmproacsa = firmaEmproacsa;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getDirigido() {
        return dirigido;
    }

    public void setDirigido(String dirigido) {
        this.dirigido = dirigido;
    }

    public String getSubcontratista() {
        return subcontratista;
    }

    public void setSubcontratista(String subcontratista) {
        this.subcontratista = subcontratista;
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public String getPromotor() {
        return promotor;
    }

    public void setPromotor(String promotor) {
        this.promotor = promotor;
    }

    public Boolean getPromotorB() {
        return promotorB;
    }

    public void setPromotorB(Boolean promotorB) {
        this.promotorB = promotorB;
    }

    public Boolean getContratista() {
        return contratista;
    }

    public void setContratista(Boolean contratista) {
        this.contratista = contratista;
    }

    public Boolean getDireccion() {
        return direccion;
    }

    public void setDireccion(Boolean direccion) {
        this.direccion = direccion;
    }

    public Boolean getSubcontratistaB() {
        return subcontratistaB;
    }

    public void setSubcontratistaB(Boolean subcontratistaB) {
        this.subcontratistaB = subcontratistaB;
    }

    public Boolean getReplanteo() {
        return replanteo;
    }

    public void setReplanteo(Boolean replanteo) {
        this.replanteo = replanteo;
    }

    public Boolean getDemolicion() {
        return demolicion;
    }

    public void setDemolicion(Boolean demolicion) {
        this.demolicion = demolicion;
    }

    public Boolean getMovimientoTierra() {
        return movimientoTierra;
    }

    public void setMovimientoTierra(Boolean movimientoTierra) {
        this.movimientoTierra = movimientoTierra;
    }

    public Boolean getCimentacion() {
        return cimentacion;
    }

    public void setCimentacion(Boolean cimentacion) {
        this.cimentacion = cimentacion;
    }

    public Boolean getEstructura() {
        return estructura;
    }

    public void setEstructura(Boolean estructura) {
        this.estructura = estructura;
    }

    public Boolean getCerramientos() {
        return cerramientos;
    }

    public void setCerramientos(Boolean cerramientos) {
        this.cerramientos = cerramientos;
    }

    public Boolean getCubierta() {
        return cubierta;
    }

    public void setCubierta(Boolean cubierta) {
        this.cubierta = cubierta;
    }

    public Boolean getAcabados() {
        return acabados;
    }

    public void setAcabados(Boolean acabados) {
        this.acabados = acabados;
    }

    public Boolean getInstalaciones() {
        return instalaciones;
    }

    public void setInstalaciones(Boolean instalaciones) {
        this.instalaciones = instalaciones;
    }

    public Boolean getOtros() {
        return otros;
    }

    public void setOtros(Boolean otros) {
        this.otros = otros;
    }

    public Boolean getCoordinadorSegSal() {
        return coordinadorSegSal;
    }

    public void setCoordinadorSegSal(Boolean coordinadorSegSal) {
        this.coordinadorSegSal = coordinadorSegSal;
    }

    public Boolean getTecnicoDtoPre() {
        return tecnicoDtoPre;
    }

    public void setTecnicoDtoPre(Boolean tecnicoDtoPre) {
        this.tecnicoDtoPre = tecnicoDtoPre;
    }

    public ArrayList<Formulario> getFormularioArrayList() {
        return formularioArrayList;
    }

    public void setFormularioArrayList(ArrayList<Formulario> formularioArrayList) {
        this.formularioArrayList = formularioArrayList;
    }
}
