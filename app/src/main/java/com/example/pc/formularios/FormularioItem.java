package com.example.pc.formularios;

/**
 * Created by PC on 10/04/2017.
 */

public class FormularioItem {
    private String codigo;
    private String actuacion;
    private String situacion;
    private String fecha;

    public FormularioItem(String codigo, String actuacion,String situacion, String fecha) {
        this.codigo = codigo;
        this.situacion = situacion;
        this.actuacion = actuacion;
        this.fecha=fecha;

    }

    public String getActuacion() {
        return actuacion;
    }

    public void setActuacion(String actuacion) {
        this.actuacion = actuacion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo= codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public String getSituacion() {
        return situacion;
    }



}
