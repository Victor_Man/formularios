package com.example.pc.formularios;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MenuActivity extends AppCompatActivity {
    private static final String ACTIVITY ="MenuActivity";
    private ArrayList<FormularioItem> items;
    public static  Context contexto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contexto=this;
        SharedPreferences pref =PreferenceManager.getDefaultSharedPreferences(this);
        boolean isSelect=pref.getBoolean("recuperarF", false);
        if(isSelect){
            SharedPreferences prefs = getSharedPreferences("MiCodigo", Context.MODE_PRIVATE);
            String codigo = prefs.getString("codigo", "");
            if(!codigo.equals("")){
                this.startActivity(new Intent(this, FormularioActivity.class));
            }
        }
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                //SharedPreferent para saber si creo uno nuevo o edito uno creado
                SharedPreferences pref = contexto.getSharedPreferences("ModoGuardarEditar", Context.MODE_PRIVATE);
                SharedPreferences.Editor edito = pref.edit();
                edito.putString("guardareditar","guardar");
                edito.commit();
                startActivity(new Intent(MenuActivity.this, FormularioActivity.class));
                finish();
            }
        });

        FormularioSQLite formularioSQLite = new FormularioSQLite(this,"formularios", null, 3);
        SQLiteDatabase db = formularioSQLite.getReadableDatabase();
        if(db!=null) {

            Cursor a = db.rawQuery("SELECT codigo, actuacion, fecha, situacion FROM vista "+ Filtro()+" ORDER BY fecha DESC" , null);
            if(a.getCount()>0) {
                if (a.moveToFirst()) {
                    items = new ArrayList<>();
                }
                do {
                    String codigo = a.getString(a.getColumnIndex("codigo"));
                    String actuacion = a.getString(a.getColumnIndex("actuacion"));
                    String fecha = a.getString(a.getColumnIndex("fecha"));
                    String situacion = a.getString(a.getColumnIndex("situacion"));
                    //SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    items.add(new FormularioItem(codigo, actuacion, situacion, fecha));
                }
                while (a.moveToNext());
                db.close();
                // Se inicializa el RecyclerView
                final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                // Se crea el Adaptador con los datos
                FormularioAdapter adaptador = new FormularioAdapter(items);

                // Se asocia el elemento con una acción al pulsar el elemento
                adaptador.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int position = recyclerView.getChildAdapterPosition(v);
                        //SharedPreferent para saber el codigo del formulario
                        SharedPreferences prefs = contexto.getSharedPreferences("MiCodigo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("codigo", items.get(position).getCodigo());
                        editor.commit();
                        //SharedPreferent para saber si creo uno nuevo o edito uno creado
                        SharedPreferences pref = contexto.getSharedPreferences("ModoGuardarEditar", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edito = pref.edit();
                        edito.putString("guardareditar","editar");
                        edito.commit();
                        Intent intent1 = new Intent(MenuActivity.this, FormularioActivity.class);
                        startActivity(intent1);
                        finish();
                    }
                });
                // Se asocia el Adaptador al RecyclerView
                recyclerView.setAdapter(adaptador);

                // Se muestra el RecyclerView en vertical
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, AjustesActivity.class));
            return true;
        }
        if (id == R.id.action_filter) {
            startActivity(new Intent(this, FiltradoActivity.class));
            finish();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public String Filtro(){

        SharedPreferences prefs = contexto.getSharedPreferences("filtro", Context.MODE_PRIVATE);
        String actuacion = prefs.getString("actuacion", "");
        String fechaI = prefs.getString("fechaI","");

        String fechaF = prefs.getString("fechaF","");

        String situacion = prefs.getString("situacion","");
        String where = "Where ";
        if((actuacion.equals(""))&&(fechaI.equals(""))&&(fechaF.equals(""))&&(situacion.equals(""))){
            where="";
        }
        if(!actuacion.equals("")){
            where= where+"actuacion LIKE '"+actuacion+"%' ";
        }
        if((!fechaI.equals(""))&&(!fechaF.equals(""))){
            if(!where.equals("Where ")){
                where =where+"AND ";
            }
            where= where+"fecha BETWEEN '"+fechaI+"' AND '"+fechaF+"' ";
            }

        if (!situacion.equals("")){
            if(!where.equals("Where ")){
                where =where+"AND ";
            }
            where= where+"situacion LIKE '"+situacion+"%' ";
        }


        return where;
    }
}
