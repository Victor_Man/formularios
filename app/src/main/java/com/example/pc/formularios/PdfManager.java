package com.example.pc.formularios;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by PC on 24/04/2017.
 */

public class PdfManager {
    private static Context mContext;
    private static final String APP_FOLDER_NAME = "com.emproacsa.pdf";
    private static final String EMPROACSA = "Emproacsa";
    private static Font catFont;
    private static Font subFont ;
    private static Font smallBold ;
    private static Font smallFont ;
    private static Font italicFont ;
    private static Font italicFontBold ;

    //Declaramos nuestra fuente base que se encuentra en la carpeta "assets/fonts" folder
    //Usaremos arialuni.ttf que permite imprimir en nuestro PDF caracteres Unicode Cirílicos (Ruso, etc)
    private static BaseFont unicode;

    //!!!Importante: La carpeta "assets/fonts/arialuni.ttf" debe estar creada en nuestro projecto en
    //la subcarpeta "PdfCreator/build/exploded-bundles/ComAndroidSupportAppcompactV71900.aar"
    //En el caso de que Android Studio la eliminara la copiamos manualmente
    //PdfCreator/build/exploded-bundles/ComAndroidSupportAppcompactV71900.aarassets/fonts/arialuni.ttf

    //Constructor set fonts and get context
    public PdfManager(Context context) throws IOException, DocumentException {

        mContext = context;
        //Creamos los distintos estilos para nuestro tipo de fuente.
        catFont = FontFactory.getFont(FontFactory.HELVETICA, 22,Font.BOLD, BaseColor.BLACK);

        subFont = FontFactory.getFont(FontFactory.HELVETICA, 16,Font.BOLD, BaseColor.BLACK);
        smallBold = FontFactory.getFont(FontFactory.HELVETICA, 12,Font.BOLD, BaseColor.BLACK);
        //FontFactory.getFont(FontFactory.HELVETICA, 28, Font.BOLD, Color.RED);
        smallFont = FontFactory.getFont(FontFactory.HELVETICA, 12,Font.NORMAL, BaseColor.BLACK);
        italicFont = FontFactory.getFont(FontFactory.HELVETICA, 12,Font.ITALIC, BaseColor.BLACK);
        italicFontBold = FontFactory.getFont(FontFactory.HELVETICA, 12,Font.ITALIC|Font.BOLD, BaseColor.BLACK);
}

    //Generando el documento PDF
    public void createPdfDocument(InvoiceObject invoiceObject) {
        try {

            //Creamos las carpetas en nuestro dispositivo, si existen las eliminamos.
            String fullFileName = createDirectoryAndFileName(invoiceObject);

            if(fullFileName.length()>0){
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream(fullFileName));

                document.open();

                //Creamos los metadatos del alchivo
                addMetaData(document);
                //Adicionamos el logo de la empresa
                addImage(document);
                //Creamos el título del documento
                addTitlePage(document, invoiceObject);
                //Creamos el contenido en form de tabla del documento
                //addInvoiceContent(document,invoiceObject.invoiceDetailsList);
                //Creamos el total de la factura del documento


                document.close();

                Toast.makeText(mContext, "PDF creado", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String createDirectoryAndFileName(InvoiceObject invoiceObject){


        String fullFileName ="";
        //Obtenemos el directorio raiz "/sdcard"
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File pdfDir = new File(extStorageDirectory + File.separator + APP_FOLDER_NAME);
        String FILENAME = "EmproacsaEjemplo.pdf";
        //Creamos la carpeta "com.emproacsa.pdf" y la subcarpeta "Emproacsa"
        try {
            if (!pdfDir.exists()) {
                pdfDir.mkdir();
            }
            File pdfSubDir = new File(pdfDir.getPath() + File.separator + EMPROACSA);

            if (!pdfSubDir.exists()) {
                pdfSubDir.mkdir();
            }

            fullFileName = Environment.getExternalStorageDirectory() + File.separator + APP_FOLDER_NAME + File.separator + EMPROACSA + File.separator + invoiceObject.codigo +FILENAME;

            File outputFile = new File(fullFileName);

            if (outputFile.exists()) {
                outputFile.delete();
            }
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext,e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return fullFileName;
    }

    //PDF library add file metadata function
    private static void addMetaData(Document document) {
        document.addTitle("Emproacsa PDF");
        document.addSubject("Prueba");
        document.addKeywords("PDF");
        document.addAuthor("Emproacsa");
        document.addCreator("Emproacsa");
    }

    //Creando el Título y los datos de la Empresa y el Cliente
    private static void addTitlePage(Document document, InvoiceObject invoiceObject)
            throws DocumentException {

        Paragraph preface = new Paragraph();
        // Adicionamos una línea en blanco
        addEmptyLine(preface, 1);
        // Adicionamos el títulos de la Factura y el número
        preface.add(new Paragraph(mContext.getResources().getString(R.string.Actuacion) +" "+ invoiceObject.actuacion,subFont));
        preface.add(new Paragraph(mContext.getResources().getString(R.string.fecha) +" "+ invoiceObject.fecha , subFont));
        preface.add(new Paragraph(mContext.getResources().getString(R.string.hora) + " "+ invoiceObject.hora , subFont));

        //Adicionamos los datos de la Empresa
        addEmptyLine(preface, 1);

        preface.add(new Paragraph(mContext.getResources().getString(R.string.promotor) +": "+  invoiceObject.promotor ,subFont));
        preface.add(new Paragraph(mContext.getResources().getString(R.string.situacion) +": "+ invoiceObject.situacion,subFont));
        preface.add(new Paragraph(mContext.getResources().getString(R.string.dirigidoa) +": "+ invoiceObject.dirigido,subFont));

        addEmptyLine(preface, 1);


        //Adicionamos el párrafo creado al documento
        document.add(preface);

        // Si queremos crear una nueva página
        //document.newPage();
    }

    //Procedimiento para crear una lines vacía
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    //Procedimiento para adicionar una imagen al documento PDF
    private static void addImage(Document document) throws IOException, DocumentException {

        Bitmap bitMap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.emproacsa);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitMap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] bitMapData = stream.toByteArray();
        Image image = Image.getInstance(bitMapData);
        //Posicionamos la imagen el el documento
        image.setAbsolutePosition(400f, 650f);
        document.add(image);
    }

    //Procedimiento para mostrar el documento PDF generado
    public void showPdfFile(String fileName, Context context){
        Toast.makeText(context, "Leyendo documento", Toast.LENGTH_LONG).show();

        String sdCardRoot = Environment.getExternalStorageDirectory().getPath();
        String path = sdCardRoot + File.separator + APP_FOLDER_NAME + File.separator + fileName;

        File file = new File(path);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file),"application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(intent);
        }
        catch (ActivityNotFoundException e) {
            Toast.makeText(context, "Aplicación no valida para leer PDF", Toast.LENGTH_SHORT).show();
        }
    }

    //Procedimiento para enviar por email el documento PDF generado
    public void sendPdfByEmail(String fileName, String emailTo, String emailCC, Context context){
        Toast.makeText(context, "envio email", Toast.LENGTH_LONG).show();
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Formulario de prueba");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Esto es un formulario de prueba");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailTo});
        emailIntent.putExtra(Intent.EXTRA_BCC, new String[]{emailCC});

        String sdCardRoot = Environment.getExternalStorageDirectory().getPath();
        String fullFileName = sdCardRoot + File.separator +APP_FOLDER_NAME+ File.separator + fileName;

        Uri uri = Uri.fromFile(new File(fullFileName));
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        emailIntent.setType("application/pdf");

        context.startActivity(Intent.createChooser(emailIntent, "Send email using:"));
    }

}