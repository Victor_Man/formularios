package com.example.pc.formularios;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by PC on 27/03/2017.
 */

public class FormularioSQLite extends SQLiteOpenHelper {
    //Sentencia SQL para crear la tabla de vista
    String sqlCreate = "CREATE TABLE IF NOT EXISTS `vista` (\n" +
            "  `codigo` INTEGER NOT NULL,\n" +
            "  `actuacion` varchar(60) NOT NULL ,\n" +
            "  `fecha` date NOT NULL,\n" +
            "  `hora` varchar(20) NOT NULL,\n" +
            "  `promotor` varchar(50) NOT NULL,\n" +
            "  `situacion` varchar(50) NOT NULL,\n" +
            "  `dirigido` varchar(50) NOT NULL,\n" +
            "  `promotorcheck` tinyint(1) NOT NULL,\n" +
            "  `contratista` tinyint(1) NOT NULL,\n" +
            "  `direccion` tinyint(1) NOT NULL,\n" +
            "  `subcontratista` tinyint(1) NOT NULL,\n" +
            "  `empresa_subcon` varchar(60) NOT NULL,\n" +
            "  `replanteo` tinyint(1) NOT NULL,\n" +
            "  `demolicion` tinyint(1) NOT NULL,\n" +
            "  `movimientoTierra` tinyint(1) NOT NULL,\n" +
            "  `cimentacion` tinyint(1) NOT NULL,\n" +
            "  `estructura` tinyint(1) NOT NULL,\n" +
            "  `cerramientos` tinyint(1) NOT NULL,\n" +
            "  `cubierta` tinyint(1) NOT NULL,\n" +
            "  `acabados` tinyint(1) NOT NULL,\n" +
            "  `instalaciones` tinyint(1) NOT NULL,\n" +
            "  `otros` tinyint(1) NOT NULL,\n" +
            "  `observaciones` text  NOT NULL,\n" +
            "  `coordinadorSegSal` tinyint(1) NOT NULL,\n" +
            "  `tecnicoDtoPre` tinyint(1) NOT NULL,\n" +
            "  `firmaEmproacsa` varchar(60) NOT NULL,\n" +
            "  `firmaRepresentante` varchar(60) NOT NULL,\n" +
            "  `firmaResponsable` varchar(60) NOT NULL,\n" +
            "  `urlfirmaEmproacsa` varchar(100) NOT NULL,\n" +
            "  `urlfirmaRepresentante` varchar(100) NOT NULL,\n" +
            "  `urlfirmaResponsable` varchar(100) NOT NULL,\n" +
            "  PRIMARY KEY (`codigo`)\n" +
            ") ;";

    public FormularioSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Se ejecuta la sentencia SQL de creación de la tabla
        db.execSQL(sqlCreate);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //NOTA: Por simplicidad del ejemplo aquí utilizamos directamente la opción de
        //      eliminar la tabla anterior y crearla de nuevo vacía con el nuevo formato.
        //      Sin embargo lo normal será que haya que migrar datos de la tabla antigua
        //      a la nueva, por lo que este método debería ser más elaborado.

        //Se elimina la versión anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS vista");
        //Se crea la nueva versión de la tabla
        db.execSQL(sqlCreate);
    }
}
