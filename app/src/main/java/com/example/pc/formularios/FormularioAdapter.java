package com.example.pc.formularios;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by PC on 10/04/2017.
 */

public class FormularioAdapter extends RecyclerView.Adapter<FormularioAdapter.FormularioViewHolder> implements View.OnClickListener{

    private ArrayList<FormularioItem> items;
    private View.OnClickListener listener;
    public static class FormularioViewHolder extends RecyclerView.ViewHolder {

        private TextView TextView_actuacion;
        private TextView TextView_fecha;
        private TextView TextView_situacion;

        public FormularioViewHolder(View itemView) {
            super(itemView);

            TextView_actuacion = (TextView) itemView.findViewById(R.id.TextViewactuacion);
            TextView_fecha = (TextView) itemView.findViewById(R.id.TextViewfecha);
            TextView_situacion = (TextView) itemView.findViewById(R.id.TextViewsituacion);
        }

        public void FormularioBind(FormularioItem item) {

            TextView_actuacion.setText(item.getActuacion());
            TextView_fecha.setText(item.getFecha());
            TextView_situacion.setText(item.getSituacion());
        }
    }

    // Contruye el objeto adaptador recibiendo la lista de datos
    public FormularioAdapter(@NonNull ArrayList < FormularioItem > items) {
        this.items = items;
    }


    // Se encarga de crear los nuevos objetos ViewHolder necesarios para los elementos de la colección.
    // Infla la vista del layout y crea y devuelve el objeto ViewHolder
    @Override
    public FormularioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rowlistaformularios, parent, false);
        row.setOnClickListener(this);
        FormularioViewHolder fvh = new FormularioViewHolder(row);
        return fvh;
    }

    // Se encarga de actualizar los datos de un ViewHolder ya existente.
    @Override
    public void onBindViewHolder(FormularioViewHolder viewHolder, int position) {
        FormularioItem item = items.get(position);
        viewHolder.FormularioBind(item);
    }
    // Indica el número de elementos de la colección de datos.
    @Override
    public int getItemCount() {
        return  items.size();
    }
    // Asigna un listener
    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null)
            listener.onClick(view);
    }
}
