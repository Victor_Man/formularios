package com.example.pc.formularios;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class FiltradoActivity extends AppCompatActivity {
    EditText Situaciontx, FechaIn , FechaFin, Actuaciontx;
    Button Filtrarbt , BorrarFiltradobt;
    ImageButton fechaIFbt, fechaFFbt;
    Context context;
    //Formato de la fecha
    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yy");
    Calendar Date = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtrado);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
       //-----------------------------------------------INICIALIZAR---------------------------------------------------------------
        context=this;
        Actuaciontx=(EditText) findViewById(R.id.actuacionet);
        Situaciontx = (EditText) findViewById(R.id.situacionF);
        fechaFFbt = (ImageButton) findViewById(R.id.fechaFFbt);
        fechaIFbt = (ImageButton) findViewById(R.id.fechaIFbt);
        Filtrarbt = (Button) findViewById(R.id.Filtrarbt);
        BorrarFiltradobt = (Button) findViewById(R.id.BorrarFiltrobt);
        FechaIn =(EditText) findViewById(R.id.fechaIF);
        FechaFin= (EditText) findViewById(R.id.fechaFF);
        FechaIn.setEnabled(false);
        FechaFin.setEnabled(false);
        //-------------------------------------------------------------------------------------------------------------------------
        //------------------------------------------------RECUPERA-LOS-DATOS-----------------------------------------------------
        SharedPreferences prefs = this.getSharedPreferences("filtro", Context.MODE_PRIVATE);
        String actuacion = prefs.getString("actuacion", "");
        Actuaciontx.setText(actuacion);
        String fechaI = prefs.getString("fechaI","");
        FechaIn.setText(fechaI);
        String fechaF = prefs.getString("fechaF","");
        FechaFin.setText(fechaF);
        String situacion =prefs.getString("situacion","");
        Situaciontx.setText(situacion);
        //--------------------------------------------------------------------------------------------------------------------------------
        //----------------------------------------------------FILTRAR-BOTONES-------------------------------------------------------------

        //Al pulsar llamara al metodo que abre el DatePickerDialog de la fecha inicio
        fechaIFbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDateIn();
            }
        });
        //Al pulsar llamara al metodo que abre el DatePickerDialog de la fecha fin
        fechaFFbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDateFin();
            }
        });

        Filtrarbt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if((FechaIn.getText().toString().length()<=0 && FechaFin.getText().toString().length()>0 )||(FechaIn.getText().toString().length()>0 && FechaFin.getText().toString().length()<=0)){
                    AlertDialog.Builder alerta = new AlertDialog.Builder(context);
                    alerta.setTitle(R.string.error);
                    alerta.setMessage("Campos fecha inconcluyentes");
                    alerta.show();
                }else {
                    if(FechaIn.getText().toString().length()<=0 && FechaFin.getText().toString().length()<=0){
                        SharedPreferences pref = context.getSharedPreferences("filtro", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edito = pref.edit();
                        edito.putString("actuacion", Actuaciontx.getText().toString());
                        edito.putString("fechaI", FechaIn.getText().toString());
                        edito.putString("fechaF", FechaFin.getText().toString());
                        edito.putString("situacion", Situaciontx.getText().toString());
                        edito.commit();
                        Intent intent1 = new Intent(FiltradoActivity.this, MenuActivity.class);
                        startActivity(intent1);
                        finish();
                    }else{
                        if(getDiffDates(FechaIn.getText().toString(),FechaFin.getText().toString())>=0) {
                            SharedPreferences pref = context.getSharedPreferences("filtro", Context.MODE_PRIVATE);
                            SharedPreferences.Editor edito = pref.edit();
                            edito.putString("actuacion", Actuaciontx.getText().toString());
                            edito.putString("fechaI", FechaIn.getText().toString());
                            edito.putString("fechaF", FechaFin.getText().toString());
                            edito.putString("situacion", Situaciontx.getText().toString());
                            edito.commit();
                            Intent intent1 = new Intent(FiltradoActivity.this, MenuActivity.class);
                            startActivity(intent1);
                            finish();
                        }else {
                            AlertDialog.Builder alerta = new AlertDialog.Builder(context);
                            alerta.setTitle(R.string.error);
                            alerta.setMessage("Campos fecha inconcluyentes");
                            alerta.show();
                        }
                    }
                }
            }
        });
        BorrarFiltradobt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SharedPreferences pref = context.getSharedPreferences("filtro", Context.MODE_PRIVATE);
                SharedPreferences.Editor edito = pref.edit();
                edito.putString("actuacion","");
                edito.putString("fechaI", "");
                edito.putString("fechaF", "");
                edito.putString("situacion", "");
                edito.commit();
                Actuaciontx.setText("");
                FechaIn.setText("");
                FechaFin.setText("");
                Situaciontx.setText("");
            }
        });
    }
    //-------------------------------------------------------------------------------------------------------------------------

    //---------------------------------------------PARTE-FECHA-INICIO-----------------------------------------------------------
    // Metodo que abre el DatePinckDialog de la fecha de inicio
    private void updateDateIn(){
        new DatePickerDialog(this, d, Date.get(Calendar.YEAR),Date.get(Calendar.MONTH),Date.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            Date.set(Calendar.YEAR, year);
            Date.set(Calendar.MONTH, monthOfYear);
            Date.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateTextFechaInLabel();
        }
    };

    private void updateTextFechaInLabel(){
        FechaIn.setText(formatDate.format(Date.getTime()));
    }
    //-----------------------------FIN-----------------------------------
    // Metodo que abre el DatePinckDialog de la fecha de fin
    private void updateDateFin(){
        new DatePickerDialog(this, h, Date.get(Calendar.YEAR),Date.get(Calendar.MONTH),Date.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener h = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Date.set(Calendar.YEAR, year);
            Date.set(Calendar.MONTH, monthOfYear);
            Date.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateTextFechaFinLabel();
        }
    };

    private void updateTextFechaFinLabel(){
        FechaFin.setText(formatDate.format(Date.getTime()));
    }
    //--------------------------------------------------------------------------------------------------------------------------

    //--------------------------------------------------COMPREOBAR-FECHA-------------------------------------------------------
    public long getDiffDates(String fechaInicio, String fechaFin) {

        SimpleDateFormat Dateformat = new SimpleDateFormat("dd/MM/yy");
        Date fechaInicioD = null;
        Date fechaFinD = null;
        try {
            fechaInicioD = Dateformat.parse(fechaInicio);
            fechaFinD = Dateformat.parse(fechaFin);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar1 = new GregorianCalendar();
        calendar1.setTime(fechaInicioD);
        Calendar calendar2 = new GregorianCalendar();
        calendar2.setTime(fechaFinD);
        Date fechainicio = new Date(calendar1.getTimeInMillis());
        Date fechafin = new Date(calendar2.getTimeInMillis());
        // Total Dias (se calcula a partir de los milisegundos por día)
        long millsecsPerDay = 86400000; // Milisegundos al día
        long returnValue = (fechafin.getTime() - fechainicio.getTime()) / millsecsPerDay;
        // System.out.println("Total días: " + returnValue + " Días.");

        return returnValue;
    }

    //--------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------onBackPressed-----------------------------------------------------------
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MenuActivity.class));
        finish();
    }
    //--------------------------------------------------------------------------------------------------------------------------



}
